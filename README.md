# frontend

## frontend 项目介绍

此项目用来收集我所做过的所有前端项目，包括 HTML + CSS + JavaScript、Vue、React，TypeScript，数据结构与算法等

## 项目详情

### Coding-With-Front-end    托管在 github 上，是一个综合项目仓库，包含了 css，flutter，gof（设计模式）、js（ES6）、纯 H5 项目、TypeScirpt 等，具体内容很多，我就不细致介绍了
### cloud-myblog-mini 个人博客小程序，暂不开源（读取阿里语雀笔记 API，并展示到小程序中）
### cloud_myblog_cms_vue3 个人博客，后台管理系统 vue3 重置版
### react-demo react 项目编写
### upload-util vue3 开发的文件上传工具类【未完成】
### vue2-Travel vue2 实现的去哪网移动端布局
### vue3blog vue3 实现的前端博客
### vue_shop vue2 实现的电商后台管理系统，包括 后端 API
### web-components web 组件
### wechat-applet 微信小程序整合
### zhihu-imitate vue3 + ts 实现的知乎专栏仿写


